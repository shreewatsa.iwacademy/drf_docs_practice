from django.db import models

# Create your models here.
class UploadContent(models.Model):
    owner       = models.ForeignKey("auth.User", on_delete=models.CASCADE, null=True, blank=True)
    caption     = models.CharField(max_length=100, null=True, blank=True)
    image       = models.ImageField(upload_to='uploads/images/', max_length=None)
    file        = models.FileField(upload_to='uploads/files/')
    created_at  = models.DateTimeField(auto_now_add=True)
    thumbnail   = models.ImageField(upload_to='uploads/thumbnails/', null=True, blank=True)
    
    def __str__(self):
        if self.info:
            return self.info
        else:
            return None