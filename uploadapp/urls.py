from django.urls import path, include

from . import views

from rest_framework.routers import DefaultRouter
router = DefaultRouter()
router.register("upload", views.UploadModelViewset, basename="uploads")

urlpatterns = [
    path('api/v1/', include(router.urls)),
]
