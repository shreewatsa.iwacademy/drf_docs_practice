from rest_framework import viewsets
from rest_framework import permissions

from . import models
from . import serializers


class UploadModelViewset(viewsets.ModelViewSet):
    queryset = models.UploadContent.objects.all()
    serializer_class = serializers.UploadSerializer
    # permission_classes = [permissions.IsAuthenticatedOrReadOnly,]

    def perform_create(self, serializer):
        if self.request.user.is_authenticated:
            serializer.save(owner=self.request.user)
        else:
            serializer.save()