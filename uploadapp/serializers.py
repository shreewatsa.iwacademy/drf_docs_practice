from rest_framework import serializers

from . import models

class UploadSerializer(serializers.ModelSerializer):
    # thumbnail = serializers.ReadOnlyField()
    # extra fields.
    class Meta:
        model = models.UploadContent
        fields = ['image','file','caption']
