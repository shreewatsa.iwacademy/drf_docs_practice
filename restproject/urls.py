
from django.contrib import admin
from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static

from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse

from .routers import router

@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'snippets': reverse('snippet-list', request=request, format=format)
})

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    path("",include("quickstart.urls")),
    path("",include("blog.urls")),
    path("",include("snippets.urls")),
    path("",include("uploadapp.urls")),

    path('api/v4/', include(router.urls)),
    path('api-root/custom/', api_root),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)