from django.urls import path, include
from rest_framework.routers import DefaultRouter

from snippets import views as snippets_views
from quickstart import views as quickstart_views
 
# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'snippets', snippets_views.SnippetViewSetFinal)
router.register(r'users', quickstart_views.UserViewOnly)

