python manage.py shell

from snippets.models import Snippet
from snippets.serializers import SnippetSerializer
from rest_framework.renderers import JSONRenderer, STATICHTMLRenderer
from rest_framework.parsers import JSONParser

snippet = Snippet(code='foo = "bar"\n')
snippet.save()

snippet = Snippet(code='print("hello, world")\n')
snippet.save()

serializer = SnippetSerializer(snippet)
serializer.data

content = JSONRenderer().render(serializer.data)
content

# Deserializing

import io
stream = io.BytesIO(content)
data = JSONParser().parse(stream)

serializer = SnippetSerializer(data=data)
serializer.is_valid()       #True
serializer.validated_data
serializer.save()

print(repr(SnippetSerializer()))

It's important to remember that ModelSerializer classes don't do anything particularly magical, they are simply a shortcut for creating serializer classes:
An automatically determined set of fields.
Simple default implementations for the create() and update() methods


## Basic api testing

$ pip install httpie
$ http http://127.0.0.1:8000/snippets/      
$ http http://127.0.0.1:8000/snippets/ Accept:application/json          # Request JSON
$ http http://127.0.0.1:8000/snippets/ Accept:text/html                 # Request HTML
$ http http://127.0.0.1:8000/snippets.json --debug                      # JSON suffix
$ http http://127.0.0.1:8000/snippets.api                               # Browsable API suffix
$ http --form POST http://127.0.0.1:8000/snippets/ code="print(123)"    # Post using form data
$ http --json POST http://127.0.0.1:8000/snippets/ code="print(456)"    # Post using JSON

$ http -a admin:password123 POST http://127.0.0.1:8000/snippets/ code="print(789)"
