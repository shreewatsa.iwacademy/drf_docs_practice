from django.urls import path

from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    # Function based views

    # path('api/v1/snippets/', views.snippet_list),
    # path('api/v1/snippets/<int:pk>/', views.snippet_detail),

    # end fbv


    # Class based views
    
    path('api/v2/snippets/', views.SnippetListGeneric.as_view(), name="snippet-list"),
    path('api/v2/snippets/<int:pk>/', views.SnippetDetailGeneric.as_view(), name="snippet-detail"),
    path('api/v2/snippets/<int:pk>/highlight/',views.SnippetHighlight.as_view(), name='snippet-highlight'),

    # end cbv
    
]

urlpatterns = format_suffix_patterns(urlpatterns)