from django.urls import path

from rest_framework import renderers
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

# from snippets.views import SnippetViewSetFinal, UserViewSet, api_root

snippet_list = views.SnippetViewSetFinal.as_view({
    'get': 'list',
    'post': 'create'
})

snippet_detail = views.SnippetViewSetFinal.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

snippet_highlight = views.SnippetViewSetFinal.as_view({
    'get': 'highlight'
}, renderer_classes=[renderers.StaticHTMLRenderer])

urlpatterns = format_suffix_patterns([
    path('api/v3/snippets/', snippet_list, name='snippet-list'),
    path('api/v3/snippets/<int:pk>/', snippet_detail, name='snippet-detail'),
    path('api/v3/snippets/<int:pk>/highlight/', snippet_highlight, name='snippet-highlight'),
])
