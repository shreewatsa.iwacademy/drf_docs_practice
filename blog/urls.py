from django.urls import include, path
from rest_framework import routers
from . import views

from .models import Blog
from . import views

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [
    path("api/v1/blog/", views.Blogview.as_view(), name="blogs" )
]