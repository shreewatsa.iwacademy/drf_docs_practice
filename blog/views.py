from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .models import Blog
# Create your views here.

class Blogview(APIView):
    def get(self, request, format=None):
        blogs = Blog.objects.all()
        blog_data = {}
        for blog in blogs:
            blog_data[blog.id] = {
                "title":blog.title,
                "description":blog.description
            }
        data = request.GET.get("name")
        return Response({
            "title":"hello",
            "name":data,
            "blogs":blog_data
        })

    def post(self, request, format=None):
        data = request.data
        new_blog = Blog.objects.create(title=data.get("title"), description=data.get("description"))
        return Response({"blog":new_blog.id},status=status.HTTP_200_OK)

