from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)


urlpatterns = [
    path('v1/', include(router.urls)),
    path("api/users,", views.ListUsers.as_view()),

    path('api/v2/users/', views.UserListModel.as_view()),
    path('api/v2/users/<int:pk>/', views.UserDetailModel.as_view()),

]


# user_list = views.UserViewOnly.as_view({
#     'get': 'list'
# })

# user_detail = views.UserViewOnly.as_view({
#     'get': 'retrieve'
# })

# path('users/', user_list, name='user-list'),
# path('users/<int:pk>/', user_detail, name='user-detail')
